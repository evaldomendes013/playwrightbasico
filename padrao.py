import pytest
import unittest

from playwright.sync_api import Page


class MyTest(unittest.TestCase):

    def compras(self):
        produtos = ['Skol Litro', 'Bohemia Litro', 'Antartica Litro', 'Antartica Lata', 'Bohemia Lata']
        qtde = 1
        for n in produtos:
            self.page.type(selector=f"//mat-card-title[contains(. , '{n}')]/../../..//input", text=f'{qtde}')
            # self.page.type(selector=f"/mat-card-header[contains(.,'{n}')]/ancestor::mat-card//input", text=f'{qtde}')
            self.page.click(f"//mat-card-title[contains(. , '{n}')]/../../..//span[contains(. , 'Adicionar')]")
            qtde += 1

        total = self.page.text_content(
            selector="//div[@id='cartDiv']//div[contains(. , 'Total')]//div[contains(. , 'R$')]")

        return total

    def login(self):
        self.page.type(selector="[id='mat-input-0']", text="admin")
        self.page.type(selector="[id='mat-input-1']", text="1234")
        self.page.query_selector("//span[contains(. , 'Login')]").click()
        result = self.page.text_content(selector="//div[contains(., 'Administrador')]")
        return result

    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page
        self.page.goto("http://lojafake.vanilton.net/#/compras")



class Test(MyTest):

    def test_title_page(self):
        assert self.page.title() == 'LojaFake'

    def test_compras(self):
        total1 = self.compras()
        assert total1 == ' R$102.75 '

    def test_login(self):
        result = self.login()
        assert result == ' Administrador Logout '

