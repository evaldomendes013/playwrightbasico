from playwright.sync_api import sync_playwright
from seletools.actions import drag_and_drop


def login():
    page.type(selector="[id='mat-input-0']", text="admin")
    page.type(selector="[id='mat-input-1']", text="1234")
    page.query_selector("//span[contains(. , 'Login')]").click()


def compras():
    produtos = ['Skol Litro', 'Bohemia Litro', 'Antartica Litro', 'Antartica Lata', 'Bohemia Lata']
    qtde = 1
    for n in produtos:
        page.type(selector=f"//mat-card-header[contains(.,'{n}')]/ancestor::mat-card//input", text=f'{qtde}')
        page.click(f"//mat-card-title[contains(. , '{n}')]/../../..//span[contains(. , 'Adicionar')]")
        qtde += 1

    total = page.text_content(selector="//div[@id='cartDiv']//div[contains(. , 'Total')]//div[contains(. , 'R$')]")
    print(total)
    page.click(selector="[id='btnComprar']")


with sync_playwright() as p:
    # browser = p.chromium.launch(args=['--start-maximized'],headless=False)
    browser = p.chromium.launch(headless=False)
    page = browser.new_page()
    page.goto("http://lojafake.vanilton.net/#/compras")

    login()

    compras()

    print(page.title())

    browser.close()

